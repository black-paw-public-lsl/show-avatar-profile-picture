# Show Avatar Profile Picture

This is a script to display an avatar's profile picture on a prim face.

## Use

In order to display the current profile picture of a user,
their UUID is put into the object's description.

This script takes the UUID in the object description,
searches for the user's profile picture,
and displays it on a prim face.

If there the profile picture cannot be found,
an alternative picture is displayed on the prim face.

Help text is shown if the script cannot find a valid UUID in the object description.

## Customization

### Defines

This script was created with the assumption
the Firestorm script extensions are available,
specifically the ability to use the `#define`, `#ifdef` and  `#endif` keywords.

The author checks code out using git,
and compiles scripts directly off of her drive.

For example:

```lsl
#define TEXTURE_FACE 2
#include "black-paw-public-lsl/show-avatar-profile-picture.lsl"
```

**This is not a requirement,**
but it influences the way the script is intended to be customized.

If you do not use this method,
you can simply put your own `#define` statements at the top of the script.

### Prim Face

The prim face to display the picture on is controlled by defining `TEXTURE_FACE`.
If this is not defined,
it defaults to `1`.

For example, to display the image on face 3:

```lsl
#define TEXTURE_FACE 3
```

### Fall-Back Picture

If the user's profile picture cannot be found,
an alternative image is displayed.

To set this:

```lsl
#define NO_PICTURE "5c5546a9-157e-44a1-959a-2fcffef9b91b"
```

### Hover Text Color

Help text color is controlled by defining `HOVER_TEXT_COLOR`.
If this is not defined,
it defaults to yellow.

For example, to display red help text:

```lsl
#define HOVER_TEXT_COLOR <1, 0, 0>
```

### Owner Access Only

Owner-oly access to the touch configuration can be enabled by

```lsl
#define ACCESS_OWNER_ONLY
```
