// If you put this code directly into a script rather than using
// #include, the put your customizations here.
// Read the included README for details.



//------------------------------------------------------------------------------
// Do not change code below.
//------------------------------------------------------------------------------

// Snippets and HTTPRequest bits were taken from:
//~ RANDOM PROFILE PROJECTOR v5.4.5 by Debbie Trilling ~

// Get Profile Picture by Valentine Foxdale
// optmisation by SignpostMarv Martin
// workaround for WEB-1384 by Viktoria Dovgal:
//  try meta tag instead of img first, try img as backup in case meta breaks
string profile_key_prefix = "<meta name=\"imageid\" content=\"";
string profile_img_prefix = "<img alt=\"profile image\" src=\"http://secondlife.com/app/image/";
integer profile_key_prefix_length; // calculated from profile_key_prefix in state_entry()
integer profile_img_prefix_length; // calculated from profile_key_prefix in state_entry()

#define URL_RESIDENT(a) "http://world.secondlife.com/resident/" + (string)a

// Hover text while initializing.
#ifndef HOVER_TEXT_COLOR
#define HOVER_TEXT_COLOR <1.0, 1.0, 0.0>
#endif

// Which face to display on.
#ifndef TEXTURE_FACE
#define TEXTURE_FACE 1
#endif

// Default texture keys.
#ifndef NO_PICTURE
#define NO_PICTURE "472cd404-9342-b14f-be0f-b823f60d2d18"
#endif

// Look in the description for a UUID.
// This will allow a manager to set up multiple boards for staff.
string last_object_description;
key agent_key;

// Who is controlling the menu.
key toucher;
#define LISTEN_CHANNEL -8392389

check_description(integer force)
{
    // See if the description has changed.
    string description = llStringTrim(llGetObjectDesc(), STRING_TRIM);
    if(!force && description == last_object_description) return;

    last_object_description = description;
    string error_text = "";

    // Get the agent's key.
    key new_key = (key)description;
    if(new_key) {
        agent_key = new_key;
        perform_request();
    } else {
        error_text = error_text + "Invalid avatar UUID.\nTouch to configure.\n";
        agent_key = NULL_KEY;
        llSetTexture(NO_PICTURE, TEXTURE_FACE);
    }

    llSetText(error_text, HOVER_TEXT_COLOR, 1);
}

get_config()
{
    llListen(LISTEN_CHANNEL, "", toucher, "");
    llTextBox(toucher, " \nEnter the UUID of the avatar.\n", LISTEN_CHANNEL);
    llSetTimerEvent(60);
}

init()
{
    check_description(TRUE);
    perform_request();
}

perform_request()
{
    llHTTPRequest(URL_RESIDENT(agent_key), [HTTP_METHOD, "GET"], "");
}

default
{
    state_entry()
    {
        init();
        profile_key_prefix_length = llStringLength(profile_key_prefix);
        profile_img_prefix_length = llStringLength(profile_img_prefix);
    }

    touch_start(integer touch_num)
    {
        check_description(TRUE);

        toucher = llDetectedKey(0);

        if(llGetOwner() == toucher)
            state config;
        #ifdef ACCESS_OWNER_ONLY
        else
            return;
        #endif

        if(!llSameGroup(toucher))
        {
            llInstantMessage(toucher, "This device only responds to group members. Please wear the correct group tag.");
            return;
        }

        state config;
    }

    http_response(key req,integer stat, list met, string body)
    {
        integer s1 = llSubStringIndex(body, profile_key_prefix);
        integer s1l = profile_key_prefix_length;
        if(s1 == -1)
        { // second try
            s1 = llSubStringIndex(body, profile_img_prefix);
            s1l = profile_img_prefix_length;
        }

        if(s1 != -1)
        {
            s1 += s1l;
            key UUID=llGetSubString(body, s1, s1 + 35);

            if(UUID)
                llSetTexture(UUID, TEXTURE_FACE);
            else
                llSetTexture(NO_PICTURE, TEXTURE_FACE);
        }
        else
            llSetTexture(NO_PICTURE, TEXTURE_FACE);
    }
}

state config
{
    listen( integer channel, string name, key id, string message )
    {
        llSetObjectDesc(message);
        state default;
    }

    state_entry()
    {
        llSetText("Configuring...", HOVER_TEXT_COLOR, 1);
        get_config();
    }

    state_exit()
    {
        llSetText("", HOVER_TEXT_COLOR, 1);
        llSetTimerEvent(0);
    }

    timer()
    {
        llSay(0, "Too much time was taken choosing. Returning to normal operation.");
        state default;
    }

    touch( integer num_detected )
    {
        get_config();
    }
}
